@extends('admins.layouts.master')
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <strong class="card-title" style="margin-bottom: 0 !important;">Danh mục</strong>
                            <button type="button" id="btn-create-category" class="btn btn-danger">
                                <span class="ti-plus"></span> Tạo mới
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="custom-module" style="display: flex;justify-content: space-between;align-items: center">
                                <button class="btn btn-danger" id="delete-multi" style="margin-bottom: 15px;padding: 8px 20px;border-radius: 5px;">Xóa nhiều</button>
                                <div class="search" style="display: flex;justify-content: space-between;align-items: center">
                                    <input autocomplete="off" type="text" id="category_search" name="query" placeholder="Search Bye Name Category..." class="form-control" style="outline: none;"> <span id="btn-search" class="ti-search" style="cursor: pointer;background-color: #dd4145;width: 55px;height: 38px;display: flex;align-items: center;justify-content: center;margin-left: 8px;border-radius: 5px;border: 1px solid #dd4145;"></span>
                                </div>
                            </div>
                            <div id="dataRow">
                                @include('admins.modules.categories.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        {{--Model crud--}}
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Danh mục</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="categoryForm" action="" class="form-horizontal" enctype="multipart/form-data"
                              method="POST">
                            @csrf
                            <div class="row form-group">
                                <div class="col-12 col-md-12"><label for="text-input"
                                                                     class=" form-control-label">Tên danh
                                        mục</label></div>
                                <div class="col-12 col-md-12">
                                    <input type="text" id="category_name" name="category_name"
                                           placeholder="Tên danh mục"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 col-md-12"><label for="textarea-input"
                                                                     class=" form-control-label">Mô tả</label>
                                </div>
                                <div class="col-12 col-md-12"><textarea name="category_description"
                                                                        id="category_description"
                                                                        rows="5" placeholder="Mô tả..."
                                                                        class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 col-md-12"><label for="select" class=" form-control-label">Trạng
                                        thái</label></div>
                                <div class="col-12 col-md-12">
                                    <select name="category_status" id="category_status" class="form-control">
                                        <option value="1">Actived</option>
                                        <option value="0">Locked</option>
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm" id="btn-save">
                                    <i class="fa fa-dot-circle-o"></i> Lưu
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Đặt lại
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('ajax')
    <script src="admins/builds/modules/categories/categoryajax.js"></script>
@endsection
@section('scripts')
    <script src="admins/builds/modules/categories/categorycustom.js"></script>
@endsection
