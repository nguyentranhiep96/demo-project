<table id="category-table" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="60px"><input type="checkbox" id="selectall"/></th>
        <th>ID</th>
        <th>Tên danh mục</th>
        <th>Slug danh mục</th>
        <th>Trạng thái</th>
        <th>Ngày tạo</th>
        <th>Người tạo</th>
        <th width="160px">Hoạt động</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($categories))
        @foreach($categories as $key => $category)
            <tr id="category_{{$category->id}}">
                <td><input type="checkbox" id="category_ids" data-ids="{{ $category->id }}" class="individual"/><br></td>
                <td>{!! !empty($category->id) ? $category->id : '' !!}</td>
                <td>{!! !empty($category->category_name) ? $category->category_name : '' !!}</td>
                <td>{!! !empty($category->category_slug) ? $category->category_slug : '' !!}</td>
                <td>{!! !empty($category->category_status) ? ($category->category_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>') : '<span class="badge badge-pill badge-primary">Locked</span>' !!}</td>
                <td>{!! !empty($category->created_at) ? date('d-m-Y', strtotime($category->created_at)) : '' !!}</td>
                <td>{!! !empty($category->created_by) ? $category->created_by : '' !!}</td>
                <td width="160px">
                    <a id="edit-category" data-id="{{ $category->id }}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a id="delete-category" data-id="{{ $category->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
    @else
        <p>Data Not Found</p>
    @endif
    </tbody>
</table>
{{ $categories->links() }}
