<table id="user-table" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="60px"><input type="checkbox" id="selectall"/></th>
        <th>Ảnh</th>
        <th>Họ tên</th>
        <th>UserName</th>
        <th>Số điện thoại</th>
        <th>Địa chỉ</th>
        <th>Email</th>
        <th>Trạng thái</th>
        <th>Ngày tạo</th>
        <th>Người tạo</th>
        <th width="160px">Hoạt động</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($users))
        @foreach($users as $key => $user)
            <tr id="user_{{$user->id}}">
                <td><input type="checkbox" id="user_ids" data-ids="{{ $user->id }}" class="individual"/><br></td>
                <td><img src="{{ $user->avatar ? asset('uploads/user/profile').'/'.$user->avatar : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKhbXaYAEtIkRRBEREL0jwQUEERsDeHTjH_5vD7Ssm6JsfMLhg8Q&s' }}" alt="{{ $user->avatar }}" style="height: 60px;"></td>
                <td>{!! !empty($user->fullname) ? $user->fullname : '' !!}</td>
                <td>{!! !empty($user->name) ? $user->name : '' !!}</td>
                <td>{!! !empty($user->phone) ? $user->phone : '' !!}</td>
                <td>{!! !empty($user->address) ? $user->address : '' !!}</td>
                <td>{!! !empty($user->email) ? $user->email : '' !!}</td>
                <td>{!! !empty($user->status) ? ($user->status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>') : '<span class="badge badge-pill badge-primary">Locked</span>' !!}</td>
                <td>{!! !empty($user->created_at) ? date('d-m-Y', strtotime($user->created_at)) : '' !!}</td>
                <td>{!! !empty($user->created_by) ? $user->created_by : '' !!}</td>
                <td width="160px">
                    <a id="edit-user" data-id="{{ $user->id }}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a id="delete-user" data-id="{{ $user->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
    @else
        <p>Không tìm thấy dữ liệu</p>
    @endif
    </tbody>
</table>
{{ $users->links() }}
