@extends('admins.layouts.master')
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <strong class="card-title" style="margin-bottom: 0 !important;">Thành viên</strong>
                            <button type="button" id="btn-create-user" class="btn btn-danger">
                                <span class="ti-plus"></span> Tạo mới
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="custom-module" style="display: flex;justify-content: space-between;align-items: center">
                                <button class="btn btn-danger" id="delete-multi" style="margin-bottom: 15px;padding: 8px 20px;border-radius: 5px;">Xóa nhiều</button>
                                <div class="search" style="display: flex;justify-content: space-between;align-items: center">
                                    <input autocomplete="off" type="text" id="user_search" name="user_search" placeholder="Search By Email..." class="form-control" style="outline: none;"> <span id="btn-search" class="ti-search" style="cursor: pointer;background-color: #dd4145;width: 55px;height: 38px;display: flex;align-items: center;justify-content: center;margin-left: 8px;border-radius: 5px;border: 1px solid #dd4145;"></span>
                                </div>
                            </div>
                            <div id="dataRow">
                                @include('admins.modules.users.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        {{--Model crud--}}
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Thành viên</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#" id="userForm">
                            <div class="form-group">
                                <label for="role" class=" form-control-label">UserName <span style="color: red;font-weight: 600">*</span></label>
                                <input type="text" autocomplete="off" name="name" id="name" placeholder="Enter your username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Email <span style="color: red;font-weight: 600">*</span></label>
                                <input type="text" autocomplete="off" name="email" id="email" placeholder="Enter your useremail" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Vai trò <span style="color: red;font-weight: 600">*</span></label>
                                <select name="role_id" id="role_id" class="form-control">
                                    <option value="0">-- Chọn vai trò</option>
                                    @if($roles)
                                        @foreach($roles as $key => $value)
                                            <option value="{{ $value->id }}">{!! $value->role_label !!}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Trạng thái <span style="color: red;font-weight: 600">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Actived</option>
                                    <option value="0">Locked</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                <button type="submit" class="btn btn-primary" id="btn-save"><span class="ti-save"></span> Lưu</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('ajax')
    <script src="admins/builds/modules/users/userajax.js"></script>
@endsection
@section('scripts')
    <script src="admins/builds/modules/users/usercustom.js"></script>
@endsection
