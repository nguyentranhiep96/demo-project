<table id="brand-table" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="60px"><input type="checkbox" id="selectall"/></th>
        <th>ID</th>
        <th>Tên thương hiệu</th>
        <th>SLUG</th>
        <th>Trang thái</th>
        <th>Ngày tạo</th>
        <th>Người tạo</th>
        <th width="160px">Hoạt động</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($brands))
        @foreach($brands as $key => $brand)
            <tr id="brand_{{$brand->id}}">
                <td><input type="checkbox" id="brand_ids" data-ids="{{ $brand->id }}" class="individual"/><br></td>
                <td>{!! !empty($brand->id) ? $brand->id : '' !!}</td>
                <td>{!! !empty($brand->brand_name) ? $brand->brand_name : '' !!}</td>
                <td>{!! !empty($brand->brand_slug) ? $brand->brand_slug : '' !!}</td>
                <td>{!! !empty($brand->brand_status) ? ($brand->brand_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>') : '<span class="badge badge-pill badge-primary">Locked</span>' !!}</td>
                <td>{!! !empty($brand->created_at) ? date('d-m-Y', strtotime($brand->created_at)) : '' !!}</td>
                <td>{!! !empty($brand->created_by) ? $brand->created_by : '' !!}</td>
                <td width="160px">
                    <a id="edit-brand" data-id="{{ $brand->id }}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a id="delete-brand" data-id="{{ $brand->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
    @else
        <p>Data Not Found</p>
    @endif
    </tbody>
</table>
{{ $brands->links() }}
