@extends('admins.layouts.master')
@section('styles')
    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }

        .input-file {
            background: #ccc;
            padding: 8px;
        }

        .image-append {
            margin-top: 15px;
            max-width: 180px;
        }

        #custom_slug {
            margin-top: 10px;
        }

        #text-custom-slug {
            color: #333;
            font-weight: 600;
            font-size: 14px;
            margin-left: 10px;
        }

    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tiểu sử</strong>
                        </div>
                        <div class="card-body card-block">
                            @if(Session::has('message'))
                                <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                                    <span class="badge badge-pill badge-success">Success</span>
                                    {{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="formProfile" action="{{ route('profile.update', ['user' => $user->id]) }}" class="form-horizontal" enctype="multipart/form-data" method="POST">
                                        @method('PATCH')
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="col-12 col-md-12">
                                                        <div class="input-file">
                                                            <input type='file' onchange="readURL(this);" name="avatar" id="avatar" />
                                                        </div>
                                                        <div class="image-append">
                                                            <img id="avatar"
                                                                 src="{{ $user->avatar ? asset('uploads/user/profile/'.$user->avatar) : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKhbXaYAEtIkRRBEREL0jwQUEERsDeHTjH_5vD7Ssm6JsfMLhg8Q&s'}}"
                                                                 alt="your image" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label for="select" class=" form-control-label">Vai trò</label></div>
                                                    <div class="col-12 col-md-9">
                                                        <span class="badge badge-danger">{!! $role_label !!}</span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Họ và tên</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="fullname" name="fullname" value="{{ old('fullname', $user->fullname ? $user->fullname : '') }}" placeholder="Full Name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">UserName</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="name" name="name" value="{{ old('name', $user->name ? $user->name : '') }}" placeholder="User Name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Email</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <span class="badge badge-danger"> {{ $user->email }}</span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Số điện thoại</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="phone" name="phone" value="{{ old('phone', $user->phone ? $user->phone : '') }}" placeholder="Phone" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Địa chỉ</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <textarea name="address" id="address" cols="30" rows="5" placeholder="Address" class="form-control">{{ old('address', $user->address ? $user->address : '') }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <button type="submit" class="btn btn-primary btn-sm">
                                                        <i class="fa fa-dot-circle-o"></i> Cập nhật
                                                    </button>
                                                    <button type="reset" class="btn btn-danger btn-sm">
                                                        <i class="fa fa-ban"></i> Đặt lại
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection

