<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Forgot Password Ecommerce</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="{{ asset('/') }}">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="admins/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="admins/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="admins/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="admins/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="admins/vendors/selectFX/css/cs-skin-elastic.css">


    <link rel="stylesheet" href="admins/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }
    </style>
</head>

<body class="bg-dark">
<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                <a href="index.html">
                    <img class="align-content" src="admins/images/logo.png" alt="">
                </a>
            </div>
            <div class="login-form">
                <div class="messages">
                    @if(Session::has('message'))
                        <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                            <span class="badge badge-pill badge-success">Success</span>
                            {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->any())
                            <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                                <span class="badge badge-pill badge-success">Error</span>
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                    @endif
                </div>
                <form action="{{ route('getVerifyCode') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Nhập vào tài khoản Email</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Nhập vào Email để lấy lại mật khẩu" value="{{ old('email') }}" style="margin-bottom: 10px;">
                        <span  style="color: #000; font-weight: 600; margin-top: 10px;">Một mã code sẽ gửi về mail của bạn, dùng mã đó để lấy lại mật khẩu!!</span>
                    </div>
                    <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30" style="margin-bottom: 10px">Gửi verify code</button>
                    <a href="{{ route('page-resetPwd') }}" class="btn btn-danger btn-flat m-b-30 m-t-30">Chuyển đến trang đổi mật khẩu</a>
                    <a href="{{ route('page-login') }}" class="btn btn-danger btn-flat m-b-30 m-t-30" style="margin-top: 10px">Quay lại trang login</a>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="admins/vendors/jquery/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="admins/builds/base/baseajax.js"></script>
<script src="admins/builds/base/basecustom.js"></script>
<script src="admins/builds/modules/auth/loginajax.js"></script>
<script src="admins/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="admins/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="admins/assets/js/main.js"></script>
</body>

</html>
