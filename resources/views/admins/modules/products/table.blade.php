<table id="category-table" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="60px"><input type="checkbox" id="selectall"/></th>
        <th>Ảnh</th>
        <th>Tên sản phẩm</th>
        <th>Giá sản phẩm</th>
        <th>Giá thực tế</th>
        <th>Thương hiệu</th>
        <th>Danh mục</th>
        <th>Số lượng</th>
        <th>Trạng thái</th>
        <th width="160px">Hoạt động</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($products))
        @foreach($products as $key => $product)
            <tr id="product_{{$product->id}}">
                <td><input type="checkbox" id="product_ids" data-ids="{{ $product->id }}" class="individual"/><br></td>
                <td><img src="{{ $product->product_avatar ? asset('uploads/products').'/'.$product->product_avatar : '' }}" alt="{{ $product->product_avatar }}" style="height: 80px"></td>
                <td>{!! !empty($product->product_name) ? $product->product_name : '' !!}</td>
                <td>{!! !empty($product->product_price) ? number_format($product->product_price) : '' !!}</td>
                <td>{!! !empty($product->product_price_new) ? number_format($product->product_price_new) : '' !!}</td>
                <td>{!! $product->brand->brand_name !!}</td>
                <td>{!! $product->category->category_name !!}</td>
                <td>{!! !empty($product->product_amount) ? $product->product_amount : '' !!}</td>
                <td>{!! !empty($product->product_status) ? ($product->product_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>') : '<span class="badge badge-pill badge-primary">Locked</span>' !!}</td>
                <td width="160px">
                    <a id="edit-product" data-id="{{ $product->id }}" href="{{ route('products.edit', $product->id) }}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a id="delete-product" data-id="{{ $product->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                    <a id="input-product" data-id="{{ $product->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-plus"></i></a>
                </td>
            </tr>
        @endforeach
    @else
        <p>Data Not Found</p>
    @endif
    </tbody>
</table>
{{ $products->links() }}
