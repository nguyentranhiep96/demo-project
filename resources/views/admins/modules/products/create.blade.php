@extends('admins.layouts.master')
@section('styles')
    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }
        #custom_slug {
            margin-top: 10px;
        }
        #text-custom-slug {
            color: #333;
            font-weight: 600;
            font-size: 14px;
            margin-left: 10px;
        }
        .thumbnail{
            width: 163px;
            height: 163px;
            margin: 10px;
            float: left;
        }
        #clear{
            display:none;
            float: right;
            margin-bottom: 5px;
        }
        #result {
            display: none;
            width: 100%;
        }
        .upload-gallery {
            border: 4px dotted #cccccc;
            min-height: 200px;
            width: 100%;
            margin-top: 0;
        }
        .custom-tab nav {
            background-color: rgba(43, 34, 34, 0.03);
            border: 1px solid rgba(0,0,0,.125);
        }
        .nav-tabs {
            border-bottom: none !important;
        }
        .nav-tabs .nav-link {
            border: none !important;
            border-top-left-radius: 0 !important;
            border-top-right-radius: 0 !important;
        }
        .nav-tabs .nav-item {
            margin-bottom: 0 !important;
            color: #333;
            font-weight: 600;
        }
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            background-color: rgb(247, 247, 247);
        }
        .image-append {
            border: 1px solid #f8f8f8;
            background-color: #272c33;
        }
        .upload-gallery {
            background-image: url({{ asset('/') }}images/noimg.jpg);
            background-color: #f3f3f3;
            width: 100%;
            background-size: contain;
        }
        #clear {
            display: none;
            width: 100%;
            text-align: right;
            background: transparent;
            border: transparent;
            color: #333;
            font-weight: 600;
            text-decoration: underline;
            float: none !important;
            margin-bottom: 0 !important;
            cursor: pointer;
        }
        #nav-tabContent {
            margin-top: 30px;
            height: 570px;
            overflow-y: scroll;
            padding-right: 1rem !important;
            padding-left: 0 !important;
        }
        .custom-tab-right {
            height: 300px;
            overflow-y: scroll;
            padding-right: 0.5rem !important;
        }
        .product-module .card-header {
            padding: 8px 10px !important;
        }
        .product-module .card-body {
            padding: 10px !important;
        }
        .product-module .card-body.select-custom-height {
            height: 205px;
            overflow-y: scroll;
        }

        .tree ul {
            border-left: 1px dashed #dfdfdf;
        }

        .tree li {
            list-style: none;
            padding: 0 18px;
            cursor: pointer;
            vertical-align: middle;
            background: #fff;
        }

        .tree li:first-child {
            border-radius: 3px 3px 0 0;
        }

        .tree li:last-child {
            border-radius: 0 0 3px 3px;
        }

        .tree .active,
        .active li {
            background: #efefef;
        }

        .tree label {
            cursor: pointer;
        }

        .tree input[type=checkbox] {
            margin: -2px 6px 0 0px;
        }

        .has > label {
            color: #000;
        }

        .tree .total {
            color: #e13300;
        }

        .checkbox {
            padding: 0 20px 15px 20px;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-items: center;
        }
        .checkbox-input {
            margin-right: 10px;
        }

        .input-item {
            width: 50%;
        }
        .error_product {
            color: red;
        }
    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Thêm sản phẩm</h4>
                        </div>
                        <div class="card-body">
                            <form id="form-add-product" action="{{ route('products.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="custom-tab">
                                            <nav>
                                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link active show" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Sản phẩm</a>
                                                    <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Thông tin sản phẩm</a>
                                                    <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-contact" role="tab" aria-controls="custom-nav-contact" aria-selected="false">Thông số sản phẩm</a>
                                                </div>
                                            </nav>
                                            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                                <div class="tab-pane fade active show" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Tên sản phẩm</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="product_name" name="product_name" value="{{ old('product_name') }}"
                                                                   placeholder="Tên sản phẩm" class="form-control">
                                                            @error('product_name')
                                                                <span class="error_product">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Số lượng</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="product_amount" name="product_amount" value="{{ old('product_amount') }}"
                                                                   placeholder="Số lượng" class="form-control">
                                                            @error('product_amount')
                                                            <span class="error_product">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Giá sản phẩm</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="product_price" name="product_price" value="{{ old('product_price') }}"
                                                                   placeholder="Giá sản phẩm"
                                                                   class="form-control">
                                                            @error('product_price')
                                                                <span class="error_product">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Giá thực tế</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="product_price_new" name="product_price_new" value="{{ old('product_price_new') }}"
                                                                   placeholder="Giá thực tế"
                                                                   class="form-control">
                                                            @error('product_price_new')
                                                                <span class="error_product">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="textarea-input" class=" form-control-label">Mô tả ngắn</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <textarea name="product_info" id="product_info" rows="5" placeholder="Mô tả ngắn" class="form-control">{{ old('product_info') }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="select" class=" form-control-label">Trạng
                                                                thái</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="product_status" id="product_status" value="{{ old('product_status') }}" class="form-control">
                                                                <option value="1">Actived</option>
                                                                <option value="0">Locked</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                                    <div class="row form-group">
                                                        <div class="col col-md-12">
                                                            <label for="textarea-input" class=" form-control-label">Ảnh Gallery</label>
                                                        </div>
                                                        <div class="col col-md-12">
                                                            <input style="display: none;" id="files" type="file" value="{{ old('product_gallery') }}" name="product_gallery[]" accept="image/*" multiple="true" multiple/>
                                                            <button type="button" id="clear">Xóa tất cả</button>
                                                            <div class="upload-gallery" id="upload-gallery">
                                                                <output id="result"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-12">
                                                            <label for="textarea-input" class=" form-control-label">Chi tiết sản phẩm</label>
                                                        </div>
                                                        <div class="col col-md-12">
                                                            <textarea id="content" name="product_content">{{ old('product_content') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="custom-nav-contact" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                                                    <div class="row form-group">
                                                        <div class="col col-md-12">
                                                            <label for="textarea-input" class=" form-control-label">Thông số kĩ thuật sản phẩm</label>
                                                        </div>
                                                        <div class="col col-md-12">
                                                            <textarea id="content1" name="product_attribute">{{ old('product_attribute') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="product-avatar">
                                            <div class="form-group">
                                                <label for="">Ảnh đại diện</label>
                                                @error('product_avatar')
                                                <span class="error_product">{{ $message }}</span>
                                                @enderror
                                                <div class="upload-image">
                                                    <div class="input-file">
                                                        <input type='file' name="product_avatar"
                                                               id="pro_avatar" style="display: none"/>
                                                    </div>
                                                    <div class="image-append">
                                                        <img style="width: 100%; height: 300px;" id="avatar" src="{{ asset('admins/images/noimg.jpg') }}" alt="your image"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="custom-tab-right">
                                            <div class="product-module">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Lưa chọn</h4>
                                                    </div>
                                                    <div class="card-body select-custom-height">
                                                        <div class="row form-group">
                                                            <div class="col-12 col-md-12"><label for="select" class=" form-control-label">Danh mục sản phẩm</label></div>
                                                            <div class="col-12 col-md-12">
                                                                <select name="category_id" id="category_id" value="{{ old('category_id') }}" class="form-control">
                                                                    <option value="0">-- Chọn danh mục sản phẩm</option>
                                                                    @if($categories)
                                                                        @foreach($categories as $category)
                                                                            <option value="{{ $category->id }}">{!! $category->category_name !!}</option>
                                                                        @endforeach
                                                                    @endif
                                                                    @error('category_id')
                                                                    <span class="error_product">{{ $message }}</span>
                                                                    @enderror
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-12 col-md-12"><label for="select" class=" form-control-label">Thương hiệu sản phẩm</label></div>
                                                            <div class="col-12 col-md-12">
                                                                <select name="brand_id" id="brand_id" value="{{ old('brand_id') }}" class="form-control">
                                                                    <option value="0">-- Chọn thương hiệu sản phẩm</option>
                                                                    @if($brands)
                                                                        @foreach($brands as $brand)
                                                                            <option value="{{ $brand->id }}">{!! $brand->brand_name !!}</option>
                                                                        @endforeach
                                                                    @endif
                                                                    @error('brand_id')
                                                                        <span class="error_product">{{ $message }}</span>
                                                                    @enderror
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-footer" style="width: 100%">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Lưu
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Đặt lại
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection
@section('ajax')
    <script src="admins/builds/modules/products/productajax.js"></script>
@endsection
@section('scripts')
    <script src="admins/builds/modules/products/productcustom.js"></script>
@endsection
