@extends('admins.layouts.master')
@section('styles')
    <style>
        .search-item {
            margin: 0 8px;
        }
    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <strong class="card-title" style="margin-bottom: 0 !important;">Sản phẩm</strong>
                            <a href="{{ route('products.create') }}" id="btn-create-product" class="btn btn-danger">
                                <span class="ti-plus"></span> Tạo mới
                            </a>
                        </div>
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                                    <span class="badge badge-pill badge-success">Success</span>
                                    {{ Session::get('message') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif
                            <div class="search" style="display: flex;align-items: center; justify-content: space-between; margin-bottom: 20px">
                                <div class="search-item" style="width: 20%;">
                                    Tên sản phẩm  <input autocomplete="off" type="text" id="product_search_name" name="query_name" placeholder="Search..." class="form-control">
                                </div>
                                <div class="search-item" style="width: 30%; display: flex;">
                                    <div class="item__child" style="margin-right: 16px;">
                                        Giá sản phẩm từ  <input type="text" name="query_price" id="product_search_price" placeholder="Nhập giá ..." class="form-control">
                                    </div>
                                    <div class="item__child">
                                        đến  <input type="text" name="query_price_new" id="product_search_price_new" placeholder="Nhập giá ..." class="form-control">
                                    </div>
                                </div>
                                <div class="search-item" style="width: 20%;">
                                    Theo danh mục
                                    <select name="query_category" id="search_by_category" value="{{ old('category_id') }}" class="form-control">
                                        <option value="0">-- Search theo danh mục sản phẩm</option>
                                        @if($categories)
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{!! $category->category_name !!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="search-item" style="width: 20%;">
                                    Theo thương hiệu
                                    <select name="query_brand" id="search_by_brand" value="{{ old('brand_id') }}" class="form-control">
                                        <option value="0">-- Search theo thuong hieu sản phẩm</option>
                                        @if($brands)
                                            @foreach($brands as $brand)
                                                <option value="{{ $brand->id }}">{!! $brand->brand_name !!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="search-item" style="width: 10%;">
                                    <label for=""></label>
                                    <button type="button" id="search-multi" class="btn btn-danger" style="width: 100%">Tìm kiếm  <span class="ti-search"></span></button>
                                </div>
                            </div>
                            <div class="custom-module" style="display: flex;justify-content: space-between;align-items: center">
                                <button class="btn btn-danger" id="delete-multi" style="margin-bottom: 15px;padding: 8px 20px;border-radius: 5px;">Xóa nhiều</button>
                            </div>
                            <div id="dataRow">
                                @include('admins.modules.products.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection
@section('ajax')
    <script src="admins/builds/modules/products/productajax.js"></script>
@endsection
@section('scripts')
    <script src="admins/builds/modules/products/productcustom.js"></script>
@endsection

