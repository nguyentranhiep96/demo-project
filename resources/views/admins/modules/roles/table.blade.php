<table id="role-table" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="60px"><input type="checkbox" id="selectall"/></th>
        <th>ID</th>
        <th>Tên nhóm quyền</th>
        <th>Tên hiển thị</th>
        <th>Mô tả</th>
        <th>Trạng thái</th>
        <th>Ngày tạo</th>
        <th>Người tạo</th>
        <th width="160px">Hoạt động</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($roles))
        @foreach($roles as $key => $role)
            <tr id="role_{{$role->id}}">
                <td><input type="checkbox" id="role_ids" data-ids="{{ $role->id }}" class="individual"/><br></td>
                <td>{!! !empty($role->id) ? $role->id : '' !!}</td>
                <td>{!! !empty($role->role_name) ? $role->role_name : '' !!}</td>
                <td>{!! !empty($role->role_label) ? $role->role_label : '' !!}</td>
                <td>{!! !empty($role->role_description) ? $role->role_description : '' !!}</td>
                <td>{!! !empty($role->role_status) ? ($role->role_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>') : '<span class="badge badge-pill badge-primary">Locked</span>' !!}</td>
                <td>{!! !empty($role->created_at) ? date('d-m-Y', strtotime($role->created_at)) : '' !!}</td>
                <td>{!! !empty($role->created_by) ? $role->created_by : '' !!}</td>
                <td width="160px">
                    <a id="edit-role" data-id="{{ $role->id }}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                    <a id="delete-role" data-id="{{ $role->id }}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                    <a id="setting-permission" data-id="{{ $role->id }}" href="javascript:void(0)" class="btn btn-danger"><span class="ti-settings"></span></a>
                </td>
            </tr>
        @endforeach
    @else
        <p>Dữ liệu không tìm thấy</p>
    @endif
    </tbody>
</table>
{{ $roles->links() }}
