@extends('admins.layouts.master')
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <strong class="card-title" style="margin-bottom: 0 !important;">Nhóm quyền</strong>
                            <button type="button" id="btn-create-role" class="btn btn-danger">
                                <span class="ti-plus"></span> Tạo mới
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="custom-module" style="display: flex;justify-content: space-between;align-items: center">
                                <button class="btn btn-danger" id="delete-multi" style="margin-bottom: 15px;padding: 8px 20px;border-radius: 5px;">Xóa nhiều</button>
                                <div class="search" style="display: flex;justify-content: space-between;align-items: center">
                                    <input autocomplete="off" type="text" id="role_search" name="query" placeholder="Search By Role Display ..." class="form-control" style="outline: none;"> <span id="btn-search" class="ti-search" style="cursor: pointer;background-color: #dd4145;width: 55px;height: 38px;display: flex;align-items: center;justify-content: center;margin-left: 8px;border-radius: 5px;border: 1px solid #dd4145;"></span>
                                </div>
                            </div>
                            <div id="dataRow">
                                @include('admins.modules.roles.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->

        {{--Model crud--}}
        <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Nhóm quyền</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#" id="roleForm">
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Tên quyền <span style="color: red;font-weight: 600">*</span></label>
                                <input type="text" autocomplete="off" name="role_name" id="role_name" placeholder="Nhập vào tên quyền" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Tên hiển thị <span style="color: red;font-weight: 600">*</span></label>
                                <input type="text" autocomplete="off" name="role_label" id="role_label" placeholder="Nhập vào tên hiển thị" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="street" class=" form-control-label">Mô tả</label>
                                <textarea name="role_description" id="role_description" placeholder="Mô tả" cols="30" rows="6" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="role" class=" form-control-label">Trạng thái <span style="color: red;font-weight: 600">*</span></label>
                                <select name="role_status" id="role_status" class="form-control">
                                    <option value="1">Actived</option>
                                    <option value="0">Locked</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                <button type="submit" class="btn btn-primary" id="btn-save"><span class="ti-save"></span> Lưu </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{--Modal Seting Permission--}}
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">Cài đặt quyền</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-main" id="modal-main">
                        <form action="#" id="setPermissionForm">
                            <div class="row">
                                <div class="modal-body">
                                    <div class="select-all" style="background: #ccc;height: 40px;display: flex;align-items: center;">
                                        <div class="col-md-12">
                                            <input type="checkbox" class="per-select-all" style="margin-right: 25px;"/><span>Chọn tất cả</span>
                                        </div>
                                    </div>
                                    <div class="permission" id="permission" style="padding-top: 10px">
                                        @include('admins.modules.roles.permission')
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="modal-footer" style="width: 96%; margin: 0 auto">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                    <button type="button" class="btn btn-primary" id="btn-setting">Xác nhận</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('ajax')
    <script src="admins/builds/modules/roles/roleajax.js"></script>
@endsection
@section('scripts')
    <script src="admins/builds/modules/roles/rolecustom.js"></script>
@endsection
