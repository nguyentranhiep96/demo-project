<nav class="navbar navbar-expand-sm navbar-default">

    <div class="navbar-header">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="./"><img src="admins/images/logo.png" alt="Logo"></a>
        <a class="navbar-brand hidden" href="./"><img src="admins/images/logo2.png" alt="Logo"></a>
    </div>

    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href=""> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
            </li>
            <h3 class="menu-title">Hệ thống</h3><!-- /.menu-title -->
            <li class="active">
                <a href=""> <i class="menu-icon fa fa-users"></i>Kho hàng </a>
            </li>
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Thành viên</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="fa fa-puzzle-piece"></i><a href="{{ route('roles.index') }}">Nhóm quyền</a></li>
                    <li><i class="fa fa-id-badge"></i><a href="{{ route('users.index') }}">Quản lý thành viên</a></li>
                </ul>
            </li>
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Sản phẩm</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="fa fa-table"></i><a href="{{ route('categories.index') }}">Danh mục</a></li>
                    <li><i class="fa fa-table"></i><a href="{{ route('brands.index') }}">Thương hiệu</a></li>
                    <li><i class="fa fa-table"></i><a href="{{ route('products.index') }}">Sản phẩm</a></li>
                </ul>
            </li>
            <li class="menu-item-has-children dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Nhà cung cấp</a>
                <ul class="sub-menu children dropdown-menu">
                    <li><i class="menu-icon fa fa-th"></i><a href="">Danh sách</a></li>
                    <li><i class="menu-icon fa fa-th"></i><a href="">Sản phẩm cung cấp</a></li>
                </ul>
            </li>
            <li class="active">
                <a href=""> <i class="menu-icon fa fa-users"></i>Cài đặt web </a>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
