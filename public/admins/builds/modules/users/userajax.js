jQuery(document).ready(function ($) {
    var userForm = $('#userForm');

    function loadSwalAlert(icon, title, message) {
        Swal.fire({
            icon: icon,
            title: title,
            text: message,
        });
    }

    // Data Raw
    function dataRaw(response) {
        return `<tr id="user_${response.data.id}">
                    <td><input type="checkbox" id="user_ids" data-ids="{{ $user->id }}" class="individual"/><br></td>
                    <td><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKhbXaYAEtIkRRBEREL0jwQUEERsDeHTjH_5vD7Ssm6JsfMLhg8Q&s" alt="${response.data.avatar}" style="height: 60px;"></td>
                    <td>${response.data.fullname ? response.data.fullname : ''}</td>
                    <td>${response.data.name}</td>
                    <td>${response.data.phone ? response.data.phone : ''}</td>
                    <td>${response.data.address ? response.data.address : ''}</td>
                    <td>${response.data.email}</td>
                    <td>${response.data.status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>'}</td>
                    <td>${response.data.created_at}</td>
                    <td>${response.data.created_by}</td>
                    <td width="160px">
                        <a id="edit-user" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                        <a id="delete-user" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>`;
    }

    // show modal create
    $(document).on("click", "#btn-create-user", function (e) {
        e.preventDefault();
        $('#btn-save').val("create-user");
        $('#btn-save').html("<span class='ti-save'></span> Save");
        userForm.trigger("reset");
        $('#mediumModal').modal('show');
    });

    // edit user
    $(document).on("click", "#edit-user", function (e) {
        e.preventDefault();
        $('#btn-save').val("edit-user");
        $('#btn-save').html("<span class='ti-save'></span> Update");
        userForm.trigger("reset");
        let id = $(this).data('id');
        let urlResource = '/admin/users/' + id + '/edit';
        callAjax(urlResource, 'GET')
            .done(response => {
                if (response.code === 500) {
                    loadSwalAlert('error', '!OK', 'ID không tồn tại');
                    window.location.reload();
                } else {
                    $('#mediumModal').modal('show');
                    $('#name').val(response.data.name);
                    $('#email').val(response.data.email);
                    $('#role_id').val(response.data.role_id);
                    $('#status').val(response.data.status);
                    $('#btn-save').attr("data-id", id);
                }
            })
            .fail(error => {
                console.log(error);
            });
    });

    // create or update user
    userForm.validate({
        rules: {
            name: {
                required: true,
                minlength: 4,
                maxlength: 20,
            },
            email: {
                required: true,
                minlength: 4,
                maxlength: 25,
                email: true,
            },
        },
        messages: {
            name: {
                required: 'Vui lòng nhập username',
                minlength: 'Nhập tối thiểu 4 kí tự',
                maxlength: 'Nhập tối đa 20 kí tự',
            },
            email: {
                required: 'Vui lòng nhập useremail',
                minlength: 'Nhập tối thiểu 4 kí tự',
                maxlength: 'Nhập tối đa 25 kí tự',
                email: 'Email không đúng định dạng',
            },
        },
        submitHandler: function (form) {
            var actionType = $('#btn-save').val();
            if (actionType === "create-user") {
                let dataResource = userForm.serialize();
                let urlResource = "/admin/users";
                callAjax(urlResource, 'POST', dataResource)
                    .done(response => {
                        $('#dataRow').html(response);
                        $("#selectall").click(function () {
                            $(".individual").prop("checked", $(this).prop("checked"));
                        });
                        $('#userForm').trigger("reset");
                        $('#btn-save').html("<span class='ti-save'></span> Save");
                        loadSwalAlert('success', 'OK', 'Thêm User thành công');
                    })
                    .fail(error => {
                        if (error.responseJSON.errors.name) {
                            loadSwalAlert('error', '!OK','UserName đã tồn tại');
                        }
                        if (error.responseJSON.errors.email) {
                            loadSwalAlert('error', '!OK','Email đã tồn tại');
                        }
                    });
            } else {
                let id = $('#btn-save').data('id');
                let urlResource = "/admin/users/" + id;
                let dataResource = userForm.serialize();
                callAjax(urlResource, 'PATCH', dataResource)
                    .done(response => {
                        if (response.code === 500) {
                            loadSwalAlert('error', '!OK', 'ID không tồn tại');
                            window.location.reload();
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'OK',
                                text: 'Cập nhật User thành công',
                            })
                                .then((result) => {
                                    if (result.value) {
                                        $("#user_" + id).replaceWith(dataRaw(response));
                                        $('#mediumModal').modal('hide');
                                    }
                                })
                        }
                    })
                    .fail(error => {
                        console.log(error);
                    });
            }
        }
    });

    // delete user
    $(document).on("click", "#delete-user", function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let urlResouce = '/admin/users/' + id;
        callAjax(urlResouce, 'DELETE')
            .done(response => {
                if (response.code === 500) {
                    loadError('ID không tồn tại');
                    window.location.reload();
                } else {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $("#user_" + id).remove();
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                }
            })
            .fail(error => {
                console.log(error);
            })
    });

    // delete Multi record
    $(document).on("click", "#delete-multi", function (e) {
        e.preventDefault();
        let ids = [];
        $('#user_ids:checked').each(function (i) {
            ids.push($(this).attr('data-ids'));
        });
        if (ids.length === 0) {
            Swal.fire(
                'Cant Delete!',
                'Chọn bản ghi cần xóa.',
                'error'
            );
        } else {
            let urlResouce = '/admin/users/' + ids;
            callAjax(urlResouce, 'DELETE')
                .done(response => {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $.each(ids, function (key, value) {
                                $("#user_" + value).remove();
                            });
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                })
                .fail(error => {
                    console.log(error);
                })
        }
    });

    // search user
    $(document).on("click", "#btn-search", function (e) {
        e.preventDefault();
        let query = $('#user_search').val();
        if (query === '') {
            alert("Vui lòng nhập vào ô search");
        } else {
            let urlResource = '/admin/users/search/'+query;
            callAjax(urlResource, 'GET')
                .done(response => {
                    $('#dataRow').html(response);
                    $("#selectall").click(function () {
                        $(".individual").prop("checked", $(this).prop("checked"));
                    });
                })
                .fail(error => {
                    console.log(error);
                });
        }
    });
});

