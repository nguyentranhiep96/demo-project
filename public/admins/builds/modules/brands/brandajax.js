jQuery(document).ready(function ($) {
    var brandForm = $('#brandForm');

    function loadSwalAlert(icon, title, message) {
        Swal.fire({
            icon: icon,
            title: title,
            text: message,
        });
    }

    // Data Raw
    function dataRaw(response) {
        return `<tr id="brand_${response.data.id}">
                    <td><input type="checkbox" id="brand_ids" data-ids="${response.data.id}" class="individual"/><br></td>
                    <td>${response.data.id}</td>
                    <td>${response.data.brand_name}</td>
                    <td>${response.data.brand_slug}</td>
                    <td>${response.data.brand_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>'}</td>
                    <td>${response.data.created_at}</td>
                    <td>${response.data.created_by}</td>
                    <td width="160px">
                        <a id="edit-brand" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                        <a id="delete-brand" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>`;
    }

    // show modal create
    $(document).on("click", "#btn-create-brand", function (e) {
        e.preventDefault();
        $('#btn-save').val("create-brand");
        $('#btn-save').html("<span class='ti-save'></span> Save");
        brandForm.trigger("reset");
        $('#mediumModal').modal('show');
    });

    // edit brand
    $(document).on("click", "#edit-brand", function (e) {
        e.preventDefault();
        $('#btn-save').val("edit-brand");
        $('#btn-save').html("<span class='ti-save'></span> Update");
        brandForm.trigger("reset");
        let id = $(this).data('id');
        let urlResource = '/admin/brands/' + id + '/edit';
        callAjax(urlResource, 'GET')
            .done(response => {
                if (response.code === 500) {
                    loadSwalAlert('error', '!OK','ID Category không tồn tại');
                    window.location.reload();
                } else {
                    $('#mediumModal').modal('show');
                    $('#brand_name').val(response.data.brand_name);
                    $('#brand_description').val(response.data.brand_description);
                    $('#brand_status').val(response.data.brand_status);
                    $('#btn-save').attr("data-id", id);
                }
            })
            .fail(error => {
                console.log(error);
            });
    });

    // create or update brand
    brandForm.validate({
        rules: {
            brand_name: {
                required: true,
                minlength: 4,
                maxlength: 255,
            },
        },
        messages: {
            brand_name: {
                required: 'Vui lòng nhập tên thương hiệu',
                minlength: 'Nhập tối thiểu 4 kí tự',
                maxlength: 'Nhập tối đa 255 kí tự',
            },
        },
        submitHandler: function (form) {
            var actionType = $('#btn-save').val();
            if (actionType === "create-brand") {
                let dataResource = brandForm.serialize();
                let urlResource = "/admin/brands";
                callAjax(urlResource, 'POST', dataResource)
                    .done(response => {
                        $('#dataRow').html(response);
                        $("#selectall").click(function () {
                            $(".individual").prop("checked", $(this).prop("checked"));
                        });
                        brandForm.trigger("reset");
                        $('#btn-save').html("<span class='ti-save'></span> Save");
                        loadSwalAlert('success', 'OK', 'Thêm thương hiệu thành công');
                    })
                    .fail(error => {
                        if (error.responseJSON.errors.brand_name) {
                            loadSwalAlert('error', '!OK','Tên thương hiệu đã tồn tại');
                        }
                    });
            } else {
                let id = $('#btn-save').data('id');
                let urlResource = "/admin/brands/" + id;
                let dataResource = brandForm.serialize();
                callAjax(urlResource, 'PATCH', dataResource)
                    .done(response => {
                        if (response.code === 500) {
                            loadSwalAlert('error', '!OK','ID không tồn tại');
                            window.location.reload();
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'OK',
                                text: 'Cập nhật thương hiệu thành công',
                            })
                                .then((result) => {
                                    if (result.value) {
                                        $("#brand_" + id).replaceWith(dataRaw(response));
                                        $('#mediumModal').modal('hide');
                                    }
                                })
                        }
                    })
                    .fail(error => {
                        console.log(error);
                    });
            }
        }
    });

    // delete category
    $(document).on("click", "#delete-brand", function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let urlResouce = '/admin/brands/' + id;
        callAjax(urlResouce, 'DELETE')
            .done(response => {
                if (response.code === 500) {
                    loadError('ID không tồn tại');
                    window.location.reload();
                } else {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $("#brand_" + id).remove();
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                }
            })
            .fail(error => {
                console.log(error);
            })
    });

    // delete Multi record
    $(document).on("click", "#delete-multi", function (e) {
        e.preventDefault();
        let ids = [];
        $('#brand_ids:checked').each(function (i) {
            ids.push($(this).attr('data-ids'));
        });
        if (ids.length == 0) {
            Swal.fire(
                'Cant Delete!',
                'Chọn bản ghi cần xóa.',
                'error'
            );
        } else {
            let urlResouce = '/admin/brands/' + ids;
            callAjax(urlResouce, 'DELETE')
                .done(response => {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $.each(ids, function (key, value) {
                                $("#brand_" + value).remove();
                            });
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                })
                .fail(error => {
                    console.log(error);
                })
        }
    });

    // search brand
    $(document).on("click", "#btn-search", function (e) {
        e.preventDefault();
        let query = $('#brand_search').val();
        if (query === '') {
            alert("Vui lòng nhập vào ô search");
        } else {
            let urlResource = '/admin/brands/search/'+query;
            callAjax(urlResource, 'GET')
                .done(response => {
                    $('#dataRow').html(response);
                    $("#selectall").click(function () {
                        $(".individual").prop("checked", $(this).prop("checked"));
                    });
                })
                .fail(error => {
                    console.log(error);
                });
        }
    });
});

