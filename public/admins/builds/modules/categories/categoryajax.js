jQuery(document).ready(function ($) {
    var categoryForm = $('#categoryForm');

    function loadSwalAlert(icon, title, message) {
        Swal.fire({
            icon: icon,
            title: title,
            text: message,
        });
    }

    // Data Raw
    function dataRaw(response) {
        return `<tr id="category_${response.data.id}">
                    <td><input type="checkbox" id="category_ids" data-ids="${response.data.id}" class="individual"/><br></td>
                    <td>${response.data.id}</td>
                    <td>${response.data.category_name}</td>
                    <td>${response.data.category_slug}</td>
                    <td>${response.data.category_status == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-pill badge-primary">Locked</span>'}</td>
                    <td>${response.data.created_at}</td>
                    <td>${response.data.created_by}</td>
                    <td width="160px">
                        <a id="edit-category" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                        <a id="delete-category" data-id="${response.data.id}" href="javascript:void(0)" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>`;
    }

    // show modal create
    $(document).on("click", "#btn-create-category", function (e) {
        e.preventDefault();
        $('#btn-save').val("create-category");
        $('#btn-save').html("<span class='ti-save'></span> Save");
        categoryForm.trigger("reset");
        $('#mediumModal').modal('show');
    });

    // edit category
    $(document).on("click", "#edit-category", function (e) {
        e.preventDefault();
        $('#btn-save').val("edit-category");
        $('#btn-save').html("<span class='ti-save'></span> Update");
        categoryForm.trigger("reset");
        let id = $(this).data('id');
        let urlResource = '/admin/categories/' + id + '/edit';
        callAjax(urlResource, 'GET')
            .done(response => {
                if (response.code === 500) {
                    loadSwalAlert('error', '!OK','ID Category không tồn tại');
                    window.location.reload();
                } else {
                    $('#mediumModal').modal('show');
                    $('#category_name').val(response.data.category_name);
                    $('#category_description').val(response.data.category_description);
                    $('#category_status').val(response.data.category_status);
                    $('#btn-save').attr("data-id", id);
                }
            })
            .fail(error => {
                console.log(error);
            });
    });

    // create or update category
    categoryForm.validate({
        rules: {
            category_name: {
                required: true,
                minlength: 4,
                maxlength: 255,
            },
        },
        messages: {
            category_name: {
                required: 'Vui lòng nhập tên danh mục',
                minlength: 'Nhập tối thiểu 4 kí tự',
                maxlength: 'Nhập tối đa 255 kí tự',
            },
        },
        submitHandler: function (form) {
            var actionType = $('#btn-save').val();
            if (actionType === "create-category") {
                let dataResource = categoryForm.serialize();
                let urlResource = "/admin/categories";
                callAjax(urlResource, 'POST', dataResource)
                    .done(response => {
                        $('#dataRow').html(response);
                        $("#selectall").click(function () {
                            $(".individual").prop("checked", $(this).prop("checked"));
                        });
                        categoryForm.trigger("reset");
                        $('#btn-save').html("<span class='ti-save'></span> Save");
                        loadSwalAlert('success', 'OK', 'Thêm danh mục thành công');
                    })
                    .fail(error => {
                        if (error.responseJSON.errors.category_name) {
                            loadSwalAlert('error', '!OK','Tên danh mục đã tồn tại');
                        }
                    });
            } else {
                let id = $('#btn-save').data('id');
                let urlResource = "/admin/categories/" + id;
                let dataResource = categoryForm.serialize();
                callAjax(urlResource, 'PATCH', dataResource)
                    .done(response => {
                        if (response.code === 500) {
                            loadSwalAlert('error', '!OK','ID Category không tồn tại');
                            window.location.reload();
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'OK',
                                text: 'Cập nhật danh mục thành công',
                            })
                                .then((result) => {
                                    if (result.value) {
                                        $("#category_" + id).replaceWith(dataRaw(response));
                                        $('#mediumModal').modal('hide');
                                    }
                                })
                        }
                    })
                    .fail(error => {
                        console.log(error);
                    });
            }
        }
    });

    // delete category
    $(document).on("click", "#delete-category", function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let urlResouce = '/admin/categories/' + id;
        callAjax(urlResouce, 'DELETE')
            .done(response => {
                if (response.code === 500) {
                    loadError('ID không tồn tại');
                    window.location.reload();
                } else {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $("#category_" + id).remove();
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                }
            })
            .fail(error => {
                console.log(error);
            })
    });

    // delete Multi record
    $(document).on("click", "#delete-multi", function (e) {
        e.preventDefault();
        let ids = [];
        $('#category_ids:checked').each(function (i) {
            ids.push($(this).attr('data-ids'));
        });
        if (ids.length == 0) {
            Swal.fire(
                'Cant Delete!',
                'Chọn bản ghi cần xóa.',
                'error'
            );
        } else {
            let urlResouce = '/admin/categories/' + ids;
            callAjax(urlResouce, 'DELETE')
                .done(response => {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            $.each(ids, function (key, value) {
                                $("#category_" + value).remove();
                            });
                            $('#dataRow').html(response);
                            $("#selectall").click(function () {
                                $(".individual").prop("checked", $(this).prop("checked"));
                            });
                        }
                    })
                })
                .fail(error => {
                    console.log(error);
                })
        }
    });

    // search category
    $(document).on("click", "#btn-search", function (e) {
        e.preventDefault();
        let query = $('#category_search').val();
        if (query === '') {
            alert("Vui lòng nhập vào ô search");
        } else {
            let urlResource = '/admin/categories/search/'+query;
            callAjax(urlResource, 'GET')
                .done(response => {
                    $('#dataRow').html(response);
                    $("#selectall").click(function () {
                        $(".individual").prop("checked", $(this).prop("checked"));
                    });
                })
                .fail(error => {
                    console.log(error);
                });
        }
    });
});

