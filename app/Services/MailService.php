<?php

namespace App\Services;

use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public function sendVerifyCode($subject, $email, $message)
    {
        return Mail::to($email)->send(new SendMail($subject, $message));
    }
}
