<?php

namespace App\Repositories\modules\product;

interface ProductRepositoryInterface
{
    public function getRelationBy($relation);
}
