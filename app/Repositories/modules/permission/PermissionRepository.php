<?php

namespace App\Repositories\modules\permission;

use App\Models\Permission;
use App\Repositories\base\BaseRepository;
use App\Repositories\modules\permission\PermissionRepositoryInterface;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    public function model()
    {
        return Permission::class;
    }
}
