<?php

namespace App\Http\Controllers\api\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\products\CreateProductRequest;
use App\Repositories\modules\brand\BrandRepository;
use App\Repositories\modules\category\CategoryRepository;
use App\Repositories\modules\product\ProductRepository;
use App\Services\StatusResponse;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    use StatusResponse;
    use UploadFile;
    protected $productRepository;
    protected $categoryRepository;
    protected $brandRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository,
                                BrandRepository $brandRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->brandRepository = $brandRepository;
    }

    public function index()
    {
        $products = $this->productRepository->paginate('*', '10', 'DESC');
        $categories = $this->categoryRepository->findBy('category_status', 1)->get();
        $brands = $this->brandRepository->findBy('brand_status', 1)->get();
        return view('admins.modules.products.product', compact('products', 'categories', 'brands'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->findBy('category_status', 1)->get();
        $brands = $this->brandRepository->findBy('brand_status', 1)->get();
        return view('admins.modules.products.create', compact('categories', 'brands'));
    }

    public function store(CreateProductRequest $request)
    {
        try {
            if ($request->hasFile('product_avatar')) {
                $avatar = $this->uploadFile('uploads/products/',
                    $request->file('product_avatar'));
            } else {
                $avatar = null;
            }
            if ($request->hasFile('product_gallery')) {
                $gallery = $this->uploadMultiFile('uploads/products/',
                    $request->file('product_gallery'));
            } else {
                $gallery = null;
            }
            $this->productRepository->save([
                'product_avatar' => $avatar,
                'product_name' => $request->input('product_name'),
                'product_amount' => $request->input('product_amount'),
                'product_slug' => Str::slug($request->input('product_name')),
                'product_price' => $request->input('product_price'),
                'product_price_new' => $request->input('product_price_new'),
                'product_info' => $request->input('product_info'),
                'product_attribute' => $request->input('product_attribute'),
                'product_content' => $request->input('product_content'),
                'product_gallery' => $gallery,
                'category_id' => $request->input('category_id'),
                'brand_id' => $request->input('brand_id'),
                'product_status' => $request->input('product_status'),
                'created_by' => auth()->user()->name
            ]);
            return redirect()->route('products.index')->with('message', 'Tạo sản phẩm thành công');
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function uploadEditor(Request $request)
    {
        try {
            $uploaded = $this->uploadFileEditor('/uploads/products/editors/',
                $request->file('file'));
            return $uploaded;
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $fileName = $this->productRepository->findById($id)->product_avatar;
            $this->deleteFile('uploads/products/', $fileName);
            $ids = explode(",", $id);
            $this->productRepository->delete($ids);
            $products = $this->productRepository->paginate('*', 10, 'DESC');
            $products->setPath('admin/products');
            return view('admins.modules.products.table', compact('products'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function edit($id)
    {
        $product = $this->productRepository->findById($id);
        $categories = $this->categoryRepository->findBy('category_status', 1)->get();
        $brands = $this->brandRepository->findBy('brand_status', 1)->get();
        return view('admins.modules.products.edit', compact('product', 'categories', 'brands'));
    }
}
