<?php

namespace App\Http\Controllers\api\Brands;

use App\Http\Controllers\Controller;
use App\Http\Requests\brands\CreateBrandRequest;
use App\Http\Requests\brands\UpdateBrandRequest;
use App\Repositories\modules\brand\BrandRepository;
use App\Services\StatusResponse;

class BrandController extends Controller
{
    use StatusResponse;
    protected $brandRepository;

    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    public function index()
    {
        $brands = $this->brandRepository->paginate('*', 10, 'DESC');
        return view('admins.modules.brands.brand', compact('brands'));
    }

    public function store(CreateBrandRequest $request)
    {
        try {
            $this->brandRepository->save($request->all());
            $brands = $this->brandRepository->paginate('*', 10, 'DESC');
            $brands->setPath('admin/brands');
            return view('admins.modules.brands.table', compact('brands'));
        } catch (\Exception $e) {
            $this->responseException(500, $e);
        }
    }

    public function edit($id)
    {
        try {
            $brand = $this->brandRepository->findById($id);
            return $this->responseStatus(200, 'success', $brand);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function update(UpdateBrandRequest $request, $id)
    {
        try {
            $brand = $this->brandRepository->save($request->all(), $id);
            return $this->responseStatus(200, 'Update brand success', $brand);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $ids = explode(",", $id);
            $this->brandRepository->delete($ids);
            $brands = $this->brandRepository->paginate('*', 10, 'DESC');
            $brands->setPath('admin/brands');
            return view('admins.modules.brands.table', compact('brands'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function search($query)
    {
        $brands = $this->brandRepository->searchBrand($query);
        return view('admins.modules.brands.table', compact('brands'));
    }
}
