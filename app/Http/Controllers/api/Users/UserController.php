<?php

namespace App\Http\Controllers\api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\users\CreateUserRequest;
use App\Http\Requests\users\UpdateUserRequest;
use App\Repositories\modules\role\RoleRepository;
use App\Repositories\modules\user\UserRepository;
use App\Services\StatusResponse;


class UserController extends Controller
{
    use StatusResponse;
    protected $userRepository;
    protected $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        $users = $this->userRepository->paginateBy('*', 'level', 1, 10, 'DESC');
        $roles = $this->roleRepository->findByRole()->get();
        return view('admins.modules.users.user', compact('users', 'roles'));
    }

    public function store(CreateUserRequest $request)
    {
        try {
            $this->userRepository->save($request->all());
            $users = $this->userRepository->paginateBy('*', 'level', 1, 10, 'DESC');
            $users->setPath('admin/users');
            return view('admins.modules.users.table', compact('users'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function edit($id)
    {
        try {
            $user = $this->userRepository->findById($id);
            return $this->responseStatus(200, 'Success', $user);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = $this->userRepository->save($request->all(), $id);
            return $this->responseStatus(200, 'Success', $user);
        } catch (\Exception $e) {
            $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $ids = explode(",", $id);
            $this->userRepository->delete($ids);
            $users = $this->userRepository->paginateBy('*', 'level', 1, 10, 'DESC');
            $users->setPath('admin/users');
            return view('admins.modules.users.table', compact('users'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function search($query)
    {
        $users = $this->userRepository->searchUser($query);
        return view('admins.modules.users.table', compact('users'));
    }
}
