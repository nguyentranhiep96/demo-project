<?php

namespace App\Http\Controllers\api\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\categories\CreateCategoryRequest;
use App\Http\Requests\categories\UpdateCategoryRequest;
use App\Repositories\modules\category\CategoryRepository;
use App\Services\StatusResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use StatusResponse;
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->paginate('*', 10, 'DESC');
        return view('admins.modules.categories.category', compact('categories'));
    }

    public function store(CreateCategoryRequest $request)
    {
        try {
            $this->categoryRepository->save($request->all());
            $categories = $this->categoryRepository->paginate('*', 10, 'DESC');
            $categories->setPath('admin/categories');
            return view('admins.modules.categories.table', compact('categories'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function edit($id)
    {
        try {
            $category = $this->categoryRepository->findById($id);
            return $this->responseStatus(200, 'Success', $category);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        try {
            $category = $this->categoryRepository->save($request->all(), $id);
            return $this->responseStatus(200, 'Update Category Success', $category);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $ids = explode(",", $id);
            $this->categoryRepository->delete($ids);
            $categories = $this->categoryRepository->paginate('*', 10, 'DESC');
            $categories->setPath('admin/categories');
            return view('admins.modules.categories.table', compact('categories'));
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function search($query)
    {
        $categories = $this->categoryRepository->searchCategory($query);
        return view('admins.modules.categories.table', compact('categories'));
    }
}
