<?php

namespace App\Http\Controllers;

use App\Repositories\modules\role\RoleRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $roleRepository;
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function index(Request $request)
    {
        return view('admins.layouts.master');
    }
}
