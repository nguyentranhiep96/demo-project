<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Repositories\modules\role\RoleRepository;
use Closure;

class CheckPermissionForUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($request, Closure $next,$permissionName)
    {
        $auth_id = auth()->user()->role_id;
        $permissionByRole = $this->repository->findById($auth_id)->permissions()->get();
        $permissionNamesByRole = [];
        foreach ($permissionByRole as $permission) {
            $permissionNameByRole = $permission->permission_name;
            array_push($permissionNamesByRole, $permissionNameByRole);
        }
        $checkValidPermissionName = in_array($permissionName,$permissionNamesByRole);
        if ($checkValidPermissionName == true) {
            return $next($request);
        } else {
            return redirect()->route('page-error')->with('message', "Không có quyền truy cập ${permissionName}");
        }
    }
}
