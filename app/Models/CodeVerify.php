<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodeVerify extends Model
{
    public $timestamps = true;
    protected $table = 'code_verifies';
    protected $fillable = [
        'user_id',
        'code_verify',
        'verify_at',
    ];
    protected $hidden = ['code_verify'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
