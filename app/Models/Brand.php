<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public $timestamps = true;
    protected $table = 'brands';
    protected $fillable = [
        'brand_name',
        'brand_slug',
        'brand_description',
        'brand_status',
        'created_by'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'brand_id');
    }
}
