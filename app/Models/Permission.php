<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = true;
    protected $table = 'permissions';
    protected $fillable = [
        'permission_name',
        'permission_label',
        'permission_description',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'permission_role', 'permission_id', 'role_id');
    }
}
