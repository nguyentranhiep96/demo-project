<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = true;
    protected $table = 'categories';
    protected $fillable = [
        'category_name',
        'category_slug',
        'category_description',
        'category_status',
        'created_by'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'role_id');
    }
}
