<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'product_avatar',
        'product_amount',
        'product_name',
        'product_slug',
        'product_price',
        'product_price_new',
        'product_info',
        'product_attribute',
        'product_content',
        'product_gallery',
        'product_status',
        'category_id',
        'brand_id',
        'created_by',
    ];

    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }
}
