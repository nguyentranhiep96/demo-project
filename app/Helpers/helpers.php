<?php
namespace app\Helpers;
use App\Models\Category;
use CustomHelpers;

class helpers
{
    public static function menu($data,$parent = 0,$str = "", $select = 0){
        foreach ($data as $key => $value) {
            $id = $value->id;
            $name = $value->category_name;
            if($value->category_parent_id == $parent){
                if($select != 0 && $id == $select){
                    echo '<option class="bo_cha" value="'.$id.'" selected="selected">'.$str.$name.'</option>';
                }else{
                    echo '<option class="bo_cha" value="'.$id.'">'.$str. $name .'</option>';
                }
                CustomHelpers::menu($data,$id, $str."--", $select);
            }
        }
    }
}
