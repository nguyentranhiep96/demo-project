<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['permission_name' => 'setting-web', 'permission_label' => 'Setting Web', 'permission_description' => 'Setting Web', 'permission_level' => 0],
            ['permission_name' => 'manager-user', 'permission_label' => 'Manager User', 'permission_description' => 'Manager User', 'permission_level' => 0],
            ['permission_name' => 'manager-role', 'permission_label' => 'Manager Role', 'permission_description' => 'Manager Role', 'permission_level' => 0],

            ['permission_name' => 'view-category', 'permission_label' => 'View Category', 'permission_description' => 'View Category', 'permission_level' => 1],
            ['permission_name' => 'create-category', 'permission_label' => 'Create Category', 'permission_description' => 'Create Category', 'permission_level' => 1],
            ['permission_name' => 'update-category', 'permission_label' => 'Update Category', 'permission_description' => 'Update Category', 'permission_level' => 1],
            ['permission_name' => 'show-category', 'permission_label' => 'Show Category', 'permission_description' => 'Show Category', 'permission_level' => 1],
            ['permission_name' => 'delete-category', 'permission_label' => 'Delete Category', 'permission_description' => 'Delete Category', 'permission_level' => 1],

            ['permission_name' => 'view-brand', 'permission_label' => 'View Brand', 'permission_description' => 'View Brand', 'permission_level' => 1],
            ['permission_name' => 'create-brand', 'permission_label' => 'Create Brand', 'permission_description' => 'Create Brand', 'permission_level' => 1],
            ['permission_name' => 'update-brand', 'permission_label' => 'Update Brand', 'permission_description' => 'Update Brand', 'permission_level' => 1],
            ['permission_name' => 'show-brand', 'permission_label' => 'Show Brand', 'permission_description' => 'Show Brand', 'permission_level' => 1],
            ['permission_name' => 'delete-brand', 'permission_label' => 'Delete Brand', 'permission_description' => 'Delete Brand', 'permission_level' => 1],

            ['permission_name' => 'view-product', 'permission_label' => 'View Product', 'permission_description' => 'View Product', 'permission_level' => 1],
            ['permission_name' => 'create-product', 'permission_label' => 'Create Product', 'permission_description' => 'Create Product New', 'permission_level' => 1],
            ['permission_name' => 'update-product', 'permission_label' => 'Update Product', 'permission_description' => 'Update Product', 'permission_level' => 1],
            ['permission_name' => 'show-product', 'permission_label' => 'Show Product', 'permission_description' => 'Show Product', 'permission_level' => 1],
            ['permission_name' => 'delete-product', 'permission_label' => 'Delete Product', 'permission_description' => 'Delete Product', 'permission_level' => 1],
        ]);
    }
}
