<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'System Admin', 'email' => 'admin@admin.com', 'password' => bcrypt(12345678), 'level' => 0, 'role_id' => 1, 'status' => 1, 'created_by' => 'System Admin'],
            ['name' => 'tranhiep', 'email' => 'tranhiep@gmail.com', 'password' => bcrypt(12345678), 'level' => 1, 'role_id' => 2, 'status' => 1, 'created_by' => 'System Admin'],
        ]);
    }
}
