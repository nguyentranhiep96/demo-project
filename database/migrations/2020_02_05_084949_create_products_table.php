<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_avatar');
            $table->integer('product_amount');
            $table->string('product_name')->index()->unique();
            $table->string('product_slug')->index();
            $table->integer('product_price')->index();
            $table->integer('product_price_new')->index();
            $table->text('product_attribute')->nullable();
            $table->text('product_info')->nullable();
            $table->longText('product_content')->nullable();
            $table->text('product_gallery')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('CASCADE');
            $table->string('product_status')->default(1);
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
