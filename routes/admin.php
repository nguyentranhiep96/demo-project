<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('page-error', function () {
    return view('admins.layouts.error');
})->name('page-error');

Route::namespace('api')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::group(['middleware' => 'check-login'], function () {
            Route::get('/login', 'AuthController@login')->name('page-login');
            Route::post('/sign-in', 'AuthController@signIn');
            Route::get('/forgot-password', 'AuthController@forgotPassword')->name('page-forgotPwd');
            Route::post('/get-verify-code', 'AuthController@getVerifyCode')->name('getVerifyCode');
            Route::get('/reset-password', 'AuthController@resetPassword')->name('page-resetPwd');
            Route::post('/change-password-new', 'AuthController@changePasswordNew')->name('changepwdNew');
        });
    });
});

Route::prefix('admin')->middleware(['check-logout', 'auto-logout'])->group(function () {
    Route::get('/', 'AdminController@index')->name('page-admin');
    Route::namespace('api')->group(function () {
        Route::namespace('Auth')->group(function () {
            Route::post('/logout', 'AuthController@logout')->name('logout');
            Route::get('/profile/{user}/edit', 'AuthController@profile')->name('profile.edit');
            Route::patch('/profile/{user}', 'AuthController@updateProfile')->name('profile.update');
            Route::patch('/change-password/{user}', 'AuthController@changePassword')->name('user.changePassword');
        });

        Route::namespace('Roles')->group(function () {
            Route::prefix('roles')->group(function () {
                Route::get('search/{query}', 'RoleController@search')->name('role.search')
                    ->middleware('check-permission-user:manager-user');
                Route::get('{role}/permissions/', 'RoleController@showPermissionsByRole')
                    ->middleware('check-permission-user:manager-user');
                Route::post('{role}/setting/permissions/{permission}', 'RoleController@setPermissionsForRole')
                    ->middleware('check-permission-user:manager-user');
            });
            Route::resource('roles', 'RoleController')->middleware('check-permission-user:manager-role');
        });

        Route::namespace('Users')->group(function () {
            Route::get('users/search/{query}', 'UserController@search')->name('searchUser');
            Route::resource('users', 'UserController')->middleware('check-permission-user:manager-user');
        });

        Route::namespace('Categories')->group(function () {
            Route::get('categories/search/{query}', 'CategoryController@search')->name('searchCategory');
            Route::get('categories', 'CategoryController@index')->name('categories.index')
                ->middleware('check-permission-user:view-category');
            Route::get('categories/create', 'CategoryController@create')->name('categories.create')
                ->middleware('check-permission-user:create-category');
            Route::post('categories', 'CategoryController@store')->name('categories.store')
                ->middleware('check-permission-user:create-category');
            Route::get('categories/{category}/edit', 'CategoryController@edit')->name('categories.edit')
                ->middleware('check-permission-user:update-category');
            Route::patch('categories/{category}', 'CategoryController@update')->name('categories.update')
                ->middleware('check-permission-user:update-category');
            Route::delete('categories/{category}', 'CategoryController@destroy')->name('categories.destroy')
                ->middleware('check-permission-user:delete-category');
        });

        Route::namespace('Brands')->group(function () {
            Route::get('brands/search/{query}', 'BrandController@search')->name('searchBrand');
            Route::get('brands', 'BrandController@index')->name('brands.index')
                ->middleware('check-permission-user:view-brand');
            Route::get('brands/create', 'BrandController@create')->name('brands.create')
                ->middleware('check-permission-user:create-brand');
            Route::post('brands', 'BrandController@store')->name('brands.store')
                ->middleware('check-permission-user:create-brand');
            Route::get('brands/{brand}/edit', 'BrandController@edit')->name('brands.edit')
                ->middleware('check-permission-user:update-brand');
            Route::patch('brands/{brand}', 'BrandController@update')->name('brands.update')
                ->middleware('check-permission-user:update-brand');
            Route::delete('brands/{brand}', 'BrandController@destroy')->name('brands.destroy')
                ->middleware('check-permission-user:delete-brand');
        });

        Route::namespace('Products')->group(function () {
            Route::post('products/upload-editor', 'ProductController@uploadEditor')->name('products.uploadEditor');
            Route::get('products', 'ProductController@index')->name('products.index')
                ->middleware('check-permission-user:view-product');
            Route::get('products/create', 'ProductController@create')->name('products.create')
                ->middleware('check-permission-user:create-product');
            Route::post('products', 'ProductController@store')->name('products.store')
                ->middleware('check-permission-user:create-product');
            Route::get('products/{product}/edit', 'ProductController@edit')->name('products.edit')
                ->middleware('check-permission-user:update-product');
            Route::patch('products/{product}', 'ProductController@update')->name('products.update')
                ->middleware('check-permission-user:update-product');
            Route::delete('products/{product}', 'ProductController@destroy')->name('products.destroy')
                ->middleware('check-permission-user:delete-product');
        });
    });
});
